package com.toggleetc.greeting;

import com.sun.javafx.binding.Logging;
import org.apache.catalina.loader.WebappClassLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class ModuleA extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ModuleA.class);
    }

    public static void main(String[] args) throws ClassNotFoundException {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        System.out.println(contextClassLoader);
        ClassLoader cl = ModuleA.class.getClassLoader();
        ClassLoader parentCl = ModuleA.class.getClassLoader().getParent();


        Thread.currentThread().setContextClassLoader(cl);
        Class<?> a = Class.forName("com.toggleetc.greeting.A", true, cl);
        System.out.println(a + " : " + a.getClassLoader());

        ClassLoader classLoader = new CustomClassLoader();

        Thread.currentThread().setContextClassLoader(classLoader);
        Class<?> b = Class.forName("com.toggleetc.greeting.B", true, classLoader);
        System.out.println(b + " : " + b.getClassLoader());
        C.m();

        B.a();

//        SpringApplication.run(ModuleA.class, args);
    }
}