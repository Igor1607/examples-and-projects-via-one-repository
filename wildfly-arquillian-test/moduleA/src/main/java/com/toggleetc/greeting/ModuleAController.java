package com.toggleetc.greeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ModuleAController {

    @Autowired
    private ModuleAService moduleAService;

    @RequestMapping("/messageA")
    public String messageA() {
        return moduleAService.messageA();
    }
}
