package com.toggleetc.greeting;

public class B {
    static int a;
    static {
        System.out.println("B: " + C.class.getClassLoader());
        System.out.println("B is loaded");
        a();
    }

    public static void a() {
        System.out.println(++a);
    }
}
