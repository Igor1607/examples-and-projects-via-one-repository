package com.toggleetc.greeting;

import org.springframework.stereotype.Service;

@Service
public class ModuleAService {

    public String messageA() {
        return "messageA";
    }

    public String messageB() {
        return "messageB";
    }
}
