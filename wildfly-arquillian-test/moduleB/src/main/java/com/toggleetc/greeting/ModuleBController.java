package com.toggleetc.greeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ModuleBController {

    @Autowired
    private ModuleAService moduleAService;

    @RequestMapping("/messageB")
    public String messageB() {
        return moduleAService.messageB();
    }
}
