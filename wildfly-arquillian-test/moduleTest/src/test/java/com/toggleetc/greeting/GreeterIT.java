package com.toggleetc.greeting;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;

@SuppressWarnings("ArquillianTooManyDeployment")
@RunWith(Arquillian.class)
public class GreeterIT {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Deployment(order = 1, name = "moduleAname")
    public static WebArchive moduleA() {
        return ShrinkWrap.createFromZipFile(WebArchive.class, new File("../moduleA/target/moduleA-1.0-SNAPSHOT.war"));
    }

    @Deployment(order = 2, name = "moduleBname")
    public static WebArchive moduleB() {
        return ShrinkWrap.createFromZipFile(WebArchive.class, new File("../moduleB/target/moduleB-1.0-SNAPSHOT.war"));
    }

    @Deployment(order = 3, name = "wildflytestname")
    public static WebArchive createDeployment() {
        return ShrinkWrap.createFromZipFile(WebArchive.class, new File("target/wildflytest.war"));
    }

    @Test
    public void should_create_greeting() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();

        HttpResponse success = client.execute(new HttpGet("http://localhost:8080/wildflytest/com.toggleetc.greeting"));
        String responseBody = EntityUtils.toString(success.getEntity());
        Greeting greeting = objectMapper.readValue(responseBody, Greeting.class);

        Assert.assertEquals(1, greeting.getId());
        Assert.assertEquals("Hello, World!", greeting.getContent());
        Assert.assertEquals(SC_OK, success.getStatusLine().getStatusCode());


        HttpResponse notFound = client.execute(new HttpGet("http://localhost:8080/wildflytest/incorrect_url"));
        Assert.assertEquals(SC_NOT_FOUND, notFound.getStatusLine().getStatusCode());

        HttpResponse messageA = client.execute(new HttpGet("http://localhost:8080/moduleA-1.0-SNAPSHOT/messageA"));
        Assert.assertEquals("messageA", EntityUtils.toString(messageA.getEntity()));
        Assert.assertEquals(SC_OK, messageA.getStatusLine().getStatusCode());


        HttpResponse messageB = client.execute(new HttpGet("http://localhost:8080/moduleB-1.0-SNAPSHOT/messageB"));
        Assert.assertEquals("messageB", EntityUtils.toString(messageB.getEntity()));
        Assert.assertEquals(SC_OK, messageB.getStatusLine().getStatusCode());
    }
}
