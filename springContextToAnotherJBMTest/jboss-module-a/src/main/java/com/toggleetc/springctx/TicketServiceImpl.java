package com.toggleetc.springctx;

import org.springframework.stereotype.Service;

@Service
public class TicketServiceImpl implements TicketService {
    @Override
    public String getTicket() {
        return "ticket from TicketServiceImpl";
    }
}
