package com.toggleetc.springctx;

public interface TicketService {
    String getTicket();
}
