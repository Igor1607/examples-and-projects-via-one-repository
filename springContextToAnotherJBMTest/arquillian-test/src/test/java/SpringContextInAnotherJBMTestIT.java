import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

import static org.apache.http.HttpStatus.SC_OK;

@SuppressWarnings("ArquillianTooManyDeployment")
@RunWith(Arquillian.class)
public class SpringContextInAnotherJBMTestIT {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Deployment(order = 1, name = "jboss-module-a")
    public static WebArchive moduleA() {
        return ShrinkWrap.createFromZipFile(WebArchive.class, new File("../jboss-module-a/target/jboss-module-a-1.0-SNAPSHOT.war"));
    }

    @Deployment(order = 2, name = "jboss-module-b")
    public static WebArchive moduleB() {
        return ShrinkWrap.createFromZipFile(WebArchive.class, new File("../jboss-module-b/target/jboss-module-b-1.0-SNAPSHOT.war"));
    }

    @Test
    public void should_create_greeting() throws IOException, InterruptedException {
        HttpClient client = HttpClientBuilder.create().build();

        HttpResponse messageA = client.execute(new HttpGet("http://localhost:8080/jboss-module-b-1.0-SNAPSHOT/ticket"));
        String body = EntityUtils.toString(messageA.getEntity());
        System.out.println(body);
        Assert.assertEquals("ticket from TicketServiceImpl", body);
        Assert.assertEquals(SC_OK, messageA.getStatusLine().getStatusCode());

    }
}