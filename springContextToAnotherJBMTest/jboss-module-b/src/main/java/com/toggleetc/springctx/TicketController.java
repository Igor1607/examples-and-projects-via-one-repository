package com.toggleetc.springctx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.annotation.PostConstruct;

@RequestMapping
public class TicketController {
    @Autowired(required = false)
    TicketService ticketService;

    @PostConstruct
    public void postConstruct() {
        System.out.println("TicketController.postConstruct");
        System.out.println("TicketController.class.getClassLoader(): " + TicketController.class.getClassLoader());
        System.out.println("ticketService: " + String.valueOf(ticketService));
        System.out.println("ClassLoader of interface" + TicketService.class.getClassLoader());
        if (ticketService != null) {
            System.out.println("ticketService impl" + ticketService.getClass());
            System.out.println("ClassLoader of impl" + ticketService.getClass().getClassLoader());
        }
    }

    @ResponseBody
    @RequestMapping(path = "ticket")
    public String getTicket() {
        return ticketService.getTicket();
    }
}
