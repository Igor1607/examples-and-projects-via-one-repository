package com.toggleetc.springctx;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class Config {
    @Bean(initMethod = "postConstruct")
    public TicketController ticketController() {
        return new TicketController();
    }

    public static void main(String[] args) {
        A a = new C();
        a.a();
        a.b();
    }
}

class A {
    void a() {
        System.out.println("a");
    }

    void b() {
        System.out.println("b");
    }
}

class C extends A {
    @Override void a() {
        System.out.println("aa");
    }
}