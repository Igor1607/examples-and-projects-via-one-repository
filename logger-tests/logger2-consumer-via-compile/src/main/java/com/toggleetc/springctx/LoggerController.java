package com.toggleetc.springctx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoggerController {

    private static final Logger LOGGER = LoggerFactory.getLogger("LoggerControllerCompiled");

    @RequestMapping(path = "warn")
    public void warn(@RequestHeader String message) {
        LOGGER.warn("Compile: " + message);
    }

    @RequestMapping(path = "info")
    public void info(@RequestHeader String message) {
        LOGGER.info("Compile: " + message);
    }

    @RequestMapping(path = "debug")
    public void debug(@RequestHeader String message) {
        LOGGER.debug("Compile: " + message);
    }

    @RequestMapping(path = "error")
    public void error(@RequestHeader String message) {
        LOGGER.error("Compile: " + message);
    }
}
