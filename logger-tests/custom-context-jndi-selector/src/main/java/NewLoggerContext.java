import ch.qos.logback.classic.LoggerContext;

import java.util.concurrent.ScheduledExecutorService;

/**
 * Позволяет управлять потоками logback-а
 * Например, задать имя потока для анализа используется ли наш контекстсеектор по треддампам ,ограничить кол-во потоков
 * или использовать внешний тредпул
 */
public class NewLoggerContext extends LoggerContext {

    private ScheduledExecutorService scheduledExecutorService;

    @Override
    public synchronized ScheduledExecutorService getScheduledExecutorService() {
        if (this.scheduledExecutorService == null) {
            this.scheduledExecutorService = NewExecutorServiceUtil.newScheduledExecutorService();
        }

        return this.scheduledExecutorService;
    }

    @Override
    public synchronized void stop() {
        super.stop();
        if (this.scheduledExecutorService != null) {
            NewExecutorServiceUtil.shutdown(this.scheduledExecutorService);
            this.scheduledExecutorService = null;
        }
    }
}
