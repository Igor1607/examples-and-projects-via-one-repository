import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.classic.joran.ReconfigureOnChangeTask;
import ch.qos.logback.classic.selector.ContextSelector;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.status.InfoStatus;
import ch.qos.logback.core.status.StatusManager;
import ch.qos.logback.core.status.StatusUtil;
import ch.qos.logback.core.status.WarnStatus;
import ch.qos.logback.core.util.Duration;
import ch.qos.logback.core.util.StatusPrinter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * -Dlogback.ContextSelector=ModuleIdContextSelector
 * -Dmodule-id=logback
 * -Djava.io.tmpdir=C:\Users\Artem\IdeaProjects\logger
 * -Dlogback.configurationFile=C:\Users\Artem\IdeaProjects\logger\src\logback.xml
 */
public class ModuleIdContextSelector implements ContextSelector {

    private static final String JAVA_IO_TMPDIR = "java.io.tmpdir";
    private static final String CONFIG_FILE = "/src/logback.xml";
    private static final String MODULE_ID = "module-id";
    private static final String CONTEXT = "classpath:";
    private static final String LOGBACK_CONFIGURATION_FILE = "logback.configurationFile";
    private static final String LOCK = ".lock";
    private static final String XML = ".xml";
    private static final int MILLIS = 1000;
    private static final int CONFIG_CHANGES_WATCHER_INTERVAL_IN_MILLIS = 5000;
    private static final String RECONFIGURE_ON_CHANGE_TASK = "RECONFIGURE_ON_CHANGE_TASK";

    private final Map<String, LoggerContext> loggerContextByNameEntries = new ConcurrentHashMap<>();
    private final LoggerContext defaultContext;

    public ModuleIdContextSelector(LoggerContext context) {
        this.defaultContext = context;
    }

    public LoggerContext getDefaultLoggerContext() {
        return this.defaultContext;
    }

    public LoggerContext detachLoggerContext(String loggerContextName) {
        return this.loggerContextByNameEntries.remove(loggerContextName);
    }

    public LoggerContext getLoggerContext() {
        System.out.println("GET GET GET");
        String contextName = System.getProperty(MODULE_ID, null);

        if (contextName == null) {
            return this.defaultContext;
        }

        LoggerContext loggerContext = this.loggerContextByNameEntries.get(contextName);
        if (loggerContext == null) {
            loggerContext = new NewLoggerContext();
            loggerContext.setName(contextName);
            this.loggerContextByNameEntries.put(contextName, loggerContext);
            URL url;
            try {
                url = getURL(contextName, loggerContext);
            } catch (Exception e) {
                // В случае ошибки работы с файлами используем logback.xml из класспаса нашей библиотеки или -D
                url = getURLDefConfigFile(loggerContext);
                StatusManager statusManager = loggerContext.getStatusManager();
                statusManager.add(new WarnStatus(e.getMessage(), this));
                statusManager.add(new WarnStatus("Load configuration from " + url.toString(), this));
            }

            this.configureLoggerContextByURL(loggerContext, url);

            if (!StatusUtil.contextHasStatusListener(loggerContext)) {
                StatusPrinter.printInCaseOfErrorsOrWarnings(loggerContext);
            }
        }

        return loggerContext;
    }

    private URL getURL(String contextName, LoggerContext loggerContext) throws IOException, InterruptedException {
        String fileNameBase = getBasePath() + contextName;
        String fileName = fileNameBase + XML;
        File fileConf = new File(fileName);

        if (fileConf.exists()) {
            return fileConf.toURI().toURL();
        }

        File fileLocker = new File(fileNameBase + LOCK);
        if (fileLocker.exists()) {
            Thread.sleep(MILLIS);
        }
        boolean isNewLockCreated = fileLocker.createNewFile();

        if (!isNewLockCreated) {
            throw new RuntimeException("Can not create default file configuration because has lock file " + fileLocker.toString());
        }

        // Создание локального файла конфигурации
        try (FileOutputStream out = new FileOutputStream(fileConf)) {
            URL fileUrl = getURLDefConfigFile(loggerContext);
            byte[] data = Files.readAllBytes(new File(fileUrl.getFile()).toPath());
            out.write(data);
        }
        if (!fileLocker.delete()) {
            StatusManager statusManager = loggerContext.getStatusManager();
            statusManager.add(new WarnStatus("Сan not delete lock file " + fileLocker.toString(), this));
        }
        return fileConf.toURI().toURL();
    }

    private URL getURLDefConfigFile(LoggerContext loggerContext) {
        String external = System.getProperty(LOGBACK_CONFIGURATION_FILE);

        if (external == null) {
            return getClass().getResource(CONFIG_FILE);
        }

        try {
            if (external.startsWith(CONTEXT)) {
                return getClass().getResource(external.substring(CONTEXT.length()));
            } else {
                return new File(external).toURI().toURL();
            }
        } catch (Exception e) {
            StatusManager statusManager = loggerContext.getStatusManager();
            statusManager.add(new WarnStatus(e.getMessage(), this));
            statusManager.add(new WarnStatus("Can not get external default configuration file " + external, this));
            return getClass().getResource(CONFIG_FILE);
        }
    }

    private String getBasePath() {
        return System.getProperty(JAVA_IO_TMPDIR) + File.separator; // todo node-id
    }

    private void configureLoggerContextByURL(LoggerContext context, URL url) {
        try {
            JoranConfigurator configurator = new JoranConfigurator();
            context.reset();
            configurator.setContext(context);
            configurator.doConfigure(url);

            // Принудительная установка автоскана
            Runnable scanTask = (Runnable) context.getObject(RECONFIGURE_ON_CHANGE_TASK);
            if (scanTask == null) {
                ReconfigureOnChangeTask configChangesWatcher = new ReconfigureOnChangeTask();
                configChangesWatcher.setContext(context);
                context.putObject(RECONFIGURE_ON_CHANGE_TASK, configChangesWatcher);
                Duration duration = new Duration(CONFIG_CHANGES_WATCHER_INTERVAL_IN_MILLIS);
                StatusManager statusManager = context.getStatusManager();
                statusManager.add(new InfoStatus("Will scan for changes in [" + url + "] ", this));
                statusManager.add(new InfoStatus("Setting ReconfigureOnChangeTask scanning period to " + duration, this));
                ScheduledFuture<?> scheduledFuture = context.getScheduledExecutorService()
                        .scheduleAtFixedRate(configChangesWatcher, duration.getMilliseconds(), duration.getMilliseconds(),
                                TimeUnit.MILLISECONDS);
                context.addScheduledFuture(scheduledFuture);
            }
        } catch (JoranException e) {
            StatusManager statusManager = context.getStatusManager();
            statusManager.add(new WarnStatus(e.getMessage(), this));
        }

        StatusPrinter.printInCaseOfErrorsOrWarnings(context);
    }

    public List<String> getContextNames() {
        return new ArrayList<>(this.loggerContextByNameEntries.keySet());
    }

    public LoggerContext getLoggerContext(String name) {
        return this.loggerContextByNameEntries.get(name);
    }

}
