
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TestContextSelector {

    private static Logger logger1 = LoggerFactory.getLogger("main1");

    private static Logger logger2 = LoggerFactory.getLogger("main2");

    public static void main(String[] args) throws InterruptedException {
        while (true) {
            logger1.debug("Hello debug!");
            logger2.warn("Hello warn!");
            Thread.sleep(10000);
        }
    }

}
