import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

@SuppressWarnings("ArquillianTooManyDeployment")
@RunWith(Arquillian.class)
public class Logger2TestIT {

    private static final String COMPILED_LOGGER_CONTROLLER = "http://127.0.0.1:8080/logger2-consumer-via-compile-1.0-SNAPSHOT/";
    private static final String PROVIDED_LOGGER_CONTROLLER = "http://127.0.0.1:8080/logger2-consumer-via-provided-1.0-SNAPSHOT/";
    private static final HttpClient CLIENT = HttpClientBuilder.create().build();

    @Deployment(order = 1, name = "logger2-consumer-via-compile")
    public static WebArchive logger2ConsumerViaCompile() {
        return ShrinkWrap.createFromZipFile(WebArchive.class, new File("../logger2-consumer-via-compile/target/logger2-consumer-via-compile-1.0-SNAPSHOT.war"));
    }

    @Deployment(order = 2, name = "logger2-client")
    public static WebArchive logger2Client() {
        return ShrinkWrap.createFromZipFile(WebArchive.class, new File("../logger2-client/target/logger2-client-1.0-SNAPSHOT.war"));
    }

    @Deployment(order = 3, name = "logger2-consumer-via-provided")
    public static WebArchive logger2ConsumerViaProvided() {
        return ShrinkWrap.createFromZipFile(WebArchive.class, new File("../logger2-consumer-via-provided/target/logger2-consumer-via-provided-1.0-SNAPSHOT.war"));
    }

    @Test
    public void should_create_greeting() throws IOException {
        {

            HttpGet errorMessageInCompiledLogger = new HttpGet(COMPILED_LOGGER_CONTROLLER + "error");
            errorMessageInCompiledLogger.addHeader("message", "message error from compiled logger");

            HttpGet warnMessageInCompiledLogger = new HttpGet(COMPILED_LOGGER_CONTROLLER + "info");
            warnMessageInCompiledLogger.addHeader("message", "message info from compiled logger");

            HttpGet infoMessageInCompiledLogger = new HttpGet(COMPILED_LOGGER_CONTROLLER + "warn");
            infoMessageInCompiledLogger.addHeader("message", "message warn from compiled logger");

            HttpGet debugMessageInCompiledLogger = new HttpGet(COMPILED_LOGGER_CONTROLLER + "debug");
            debugMessageInCompiledLogger.addHeader("message", "message debug from compiled logger");

            CLIENT.execute(errorMessageInCompiledLogger);
            CLIENT.execute(infoMessageInCompiledLogger);
            CLIENT.execute(warnMessageInCompiledLogger);
            CLIENT.execute(debugMessageInCompiledLogger);
        }


        {
            HttpGet errorMessageInProvidedLogger = new HttpGet(PROVIDED_LOGGER_CONTROLLER + "error");
            errorMessageInProvidedLogger.addHeader("message", "message error from provided logger");

            HttpGet infoMessageInProvidedLogger = new HttpGet(PROVIDED_LOGGER_CONTROLLER + "info");
            infoMessageInProvidedLogger.addHeader("message", "message info from provided logger");

            HttpGet warnMessageInProvidedLogger = new HttpGet(PROVIDED_LOGGER_CONTROLLER + "warn");
            warnMessageInProvidedLogger.addHeader("message", "message warn from provided logger");

            HttpGet debugMessageInProvidedLogger = new HttpGet(PROVIDED_LOGGER_CONTROLLER + "debug");
            debugMessageInProvidedLogger.addHeader("message", "message debug from provided logger");

            CLIENT.execute(errorMessageInProvidedLogger);
            CLIENT.execute(infoMessageInProvidedLogger);
            CLIENT.execute(warnMessageInProvidedLogger);
            CLIENT.execute(debugMessageInProvidedLogger);
        }
    }
}