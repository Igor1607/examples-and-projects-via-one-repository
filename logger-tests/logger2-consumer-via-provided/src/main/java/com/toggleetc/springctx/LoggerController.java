package com.toggleetc.springctx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoggerController {

    private static final Logger LOGGER = LoggerFactory.getLogger("LoggerControllerProvided");

    @RequestMapping(path = "warn")
    public void warn(@RequestHeader String message) {
        LOGGER.warn("Provided: " + message);
    }

    @RequestMapping(path = "info")
    public void info(@RequestHeader String message) {
        LOGGER.info("Provided: " + message);
    }

    @RequestMapping(path = "debug")
    public void debug(@RequestHeader String message) {
        LOGGER.debug("Provided: " + message);
    }

    @RequestMapping(path = "error")
    public void error(@RequestHeader String message) {
        LOGGER.error("Provided: " + message);
    }
}
