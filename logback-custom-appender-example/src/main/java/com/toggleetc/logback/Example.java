package com.toggleetc.logback;

import com.toggleetc.logback.appender.MapAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Example {

    private static final Logger logger = LoggerFactory.getLogger(Example.class);
    private static final Logger foobar = LoggerFactory.getLogger("com.baeldung.foobar");
    private static final Logger logback = LoggerFactory.getLogger("com.baeldung.logback");
    private static final Logger testslogger = LoggerFactory.getLogger("com.baeldung.logback.tests");

    public static void main(String[] args) {
        logger.error("Example log from {}", Example.class.getSimpleName());
        logger.warn("Example log from {}", Example.class.getSimpleName());
        logger.info("Example log from {}", Example.class.getSimpleName());
        logger.debug("Example log from {}", Example.class.getSimpleName());
        logger.trace("Example log from {}", Example.class.getSimpleName());


        foobar.debug("This is logged from foobar");
        logback.debug("This is not logged from logger");
        logback.info("This is logged from logger");
        testslogger.info("This is not logged from tests");
        testslogger.warn("This is logged from tests");

        System.out.println(MapAppender.eventMap);
    }
}
