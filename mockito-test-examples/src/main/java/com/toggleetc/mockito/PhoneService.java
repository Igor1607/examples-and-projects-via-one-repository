package com.toggleetc.mockito;

import org.springframework.stereotype.Service;

@Service
public class PhoneService {


    public String realMethodA() {
        return "realMethodA";
    }

    public String realMethodB() {
        return "realMethodB";
    }

    public String realMethodC() {
        return "realMethodC";
    }

}
