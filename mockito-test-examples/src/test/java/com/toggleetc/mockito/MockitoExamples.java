package com.toggleetc.mockito;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class MockitoExamples {

    @Test
    public void test() {
        PhoneService phoneService = new PhoneService();
        PhoneService spy = Mockito.spy(phoneService);
        PhoneService mock = Mockito.mock(PhoneService.class);

        when(mock.realMethodA()).thenCallRealMethod();
        when(spy.realMethodA()).thenReturn("spyMethodA");

        System.out.println(phoneService.realMethodA());
        System.out.println(spy.realMethodA());
        System.out.println(mock.realMethodA());
        System.out.println(mock.realMethodB());
    }
}
